package dev.kropla.garage;

public class DoorOpenedState implements DoorState {

    @Override
    public void buttonPressedEvent(DoorStateContext context) {
        context.setState(new DoorClosingState());
        context.closeOneStep();
    }
}