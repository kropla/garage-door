package dev.kropla.garage;

public class DoorClosingState implements DoorState {

    @Override
    public void noEvent(DoorStateContext context) {
        context.closeOneStep();
        if (context.isDoorClosed()) {
            context.setState(new DoorClosedState());
        }
    }

    @Override
    public void buttonPressedEvent(DoorStateContext context) {
        context.setState(new DoorPausedState(this));
    }

    @Override
    public void obstacleDetected(DoorStateContext context) {
        context.openOneStep();
        context.setState(new DoorOpeningState());
    }
}
