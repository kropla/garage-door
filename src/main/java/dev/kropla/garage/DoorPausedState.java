package dev.kropla.garage;

public class DoorPausedState implements DoorState {

    private DoorState doorState;

    public DoorPausedState(DoorState doorState) {
        this.doorState = doorState;
    }

    @Override
    public void buttonPressedEvent(DoorStateContext context) {
        context.setState(doorState);
        context.noEvent();
    }


}
