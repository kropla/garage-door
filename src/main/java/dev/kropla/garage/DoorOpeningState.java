package dev.kropla.garage;

public class DoorOpeningState implements DoorState {

    @Override
    public void noEvent(DoorStateContext context) {
        context.openOneStep();
        if (context.isDoorOpened()) {
            context.setState(new DoorOpenedState());
        }
    }

    @Override
    public void buttonPressedEvent(DoorStateContext context) {
        context.setState(new DoorPausedState(this));
    }

    @Override
    public void obstacleDetected(DoorStateContext context) {
        context.closeOneStep();
        context.setState(new DoorClosingState());
    }
}
