package dev.kropla.garage;

public interface DoorState {

    default void noEvent(DoorStateContext context) {}

    void buttonPressedEvent(DoorStateContext context);

    default void obstacleDetected(DoorStateContext context) {}
}
