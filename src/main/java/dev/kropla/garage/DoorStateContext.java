package dev.kropla.garage;

public class DoorStateContext{
    private DoorState state;
    private byte doorStep = 0;

    public DoorStateContext() {
        this.state = new DoorClosedState();
    }

    public void setState(DoorState state) {
        this.state = state;
    }
    public byte noEvent() {
        state.noEvent(this);
        return doorStep;
    }

    public byte buttonPressedEvent() {
        state.buttonPressedEvent(this);
        return doorStep;
    }

    public byte obstacleDetected() {
        state.obstacleDetected(this);
        return doorStep;
    }

    public void openOneStep(){
        doorStep++;
    }

    public void closeOneStep(){
        doorStep--;
    }

    public boolean isDoorOpened(){
        return doorStep == 5;
    }

    public boolean isDoorClosed(){
        return doorStep == 0;
    }
}