package dev.kropla.garage;

import static org.junit.Assert.*;

import org.junit.Test;

/***
 * tests imported from the kata author
 * https://www.codewars.com/kata/killer-garage-door
 */
public class DoorTest {

    @Test
    public void testNormalOperation() {
        test("should stay closed unless button is pressed (no event)", "", "");
        test("should stay closed unless button is pressed (no buttonpresses)", "..........", "0000000000");
        test("should start opening on buttonpress", "P..", "123");
        test("should open completely and stay open", "P....", "12345");
        test("should open completely, then stay open, then close", "P......P......", "12345554321000");
    }

    @Test
    public void testPause() {
        test("should start opening and pause on second buttonpress", "P.P..", "12222");
        test("should resume opening on third buttonpress #1", "P.P.P....", "122234555");
        test("should resume opening on third buttonpress #2", ".....P.P........P....", "000001222222222234555");
        test("should resume closing on third buttonpress", ".....P......P.P..P....", "0000012345554333321000");
    }

    @Test
    public void testObstacles() {
        test("should reverse while opening", "P.O....", "1210000");
        test("should reverse while closing", "P......P.O....", "12345554345555");
    }

    @Test
    public void testObstaclePlusPause() {
        test("should reverse while opening (and allow pause)", "P..OP..P..", "1232222100");
        test("should reverse while closing (and allow pause)", "P......P..OP..P...", "123455543233334555");
    }

    @Test
    public void testExample() {
        test("should start opening and reverse when obstacle", "..P...O.....", "001234321000");
    }

    private void test(String description, String events, String result) {
        assertEquals(description, result, Door.run(events));
    }
}