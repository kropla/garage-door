package dev.kropla.garage;

import java.util.stream.Collectors;

public class Door {

    public static String run(String events) {
        DoorStateContext context = new DoorStateContext();
        return events.chars().map(i -> (char) i)
                .map(c -> interpretSignal(c, context))
                .mapToObj(b -> Integer.toString(b))
                .collect(Collectors.joining());
    }

    private static byte interpretSignal(int c, DoorStateContext context) {
        switch ((char) c) {
            case 'P':
                return context.buttonPressedEvent();
            case 'O':
                return context.obstacleDetected();
            default:
                return context.noEvent();
        }
    }
}