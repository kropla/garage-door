package dev.kropla.garage;

public class DoorClosedState implements DoorState {

    @Override
    public void buttonPressedEvent(DoorStateContext context) {
        context.setState(new DoorOpeningState());
        context.openOneStep();
    }
}